import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Padding buildHeader({String? titleHeader, double? left = 10.0}) {
  return Padding(
    padding: EdgeInsets.only(left: left!, bottom: 8.0),
    child: Text(
      '$titleHeader',
      style: GoogleFonts.redHatDisplay(
        color: Colors.black,
        fontSize: 18,
        fontWeight: FontWeight.w500,
      ),
    ),
  );
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
Column buildProductItem(
    {String? title, String? tutor, double? price, bool? isBestSeller, double? rating, bool? isFav}) {
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: SizedBox(
          width: Get.width * 0.8,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Image.asset('assets/frame.png', width: Get.width * 0.8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 5.0),
                        child: Container(
                            width: Get.width * 0.13,
                            height: Get.height * 0.04,
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.white),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Image.asset(
                                  'assets/rating.png',
                                  height: 15.0,
                                  width: 15.0,
                                ),
                                Text(
                                  '$rating',
                                  style: GoogleFonts.redHatDisplay(
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            )),
                      ),
                      (isFav!)
                          ? Padding(
                              padding: const EdgeInsets.only(right: 20.0, top: 5.0),
                              child: Image.asset(
                                'assets/fav.png',
                                height: Get.height * 0.08,
                                width: Get.width * 0.08,
                              ),
                            )
                          : Padding(
                              padding: const EdgeInsets.only(right: 20.0, top: 5.0),
                              child: Image.asset(
                                'assets/unfav.png',
                                height: Get.height * 0.08,
                                width: Get.width * 0.08,
                              ),
                            ),
                    ],
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  '$title',
                  style: GoogleFonts.redHatDisplay(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/people.png',
                      height: 15,
                      width: 15,
                    ),
                    Text(
                      '$tutor',
                      style: GoogleFonts.redHatDisplay(
                        color: HexColor('#A9AEB2'),
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  children: [
                    Text(
                      '\$ $price',
                      style: GoogleFonts.redHatDisplay(
                        color: HexColor('#2F80ED'),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    if (isBestSeller == true)
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Container(
                          child: Center(
                            child: Text(
                              'Best Seller',
                              style: GoogleFonts.redHatDisplay(
                                color: HexColor('#FF6666'),
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          width: 70.0,
                          height: 20.0,
                          decoration:
                              BoxDecoration(borderRadius: BorderRadius.circular(50.0), color: HexColor('#FCE2EA')),
                        ),
                      )
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    ],
  );
}

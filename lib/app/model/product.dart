class Product {
  String? title;
  String? tutor;
  double? price;
  bool? isBestSeller;
  double? rating;
  bool? isFav;

  Product({this.title, this.tutor, this.price, this.isBestSeller, this.rating, this.isFav});
}
import 'package:flutter/cupertino.dart';

class CategoryProduct {
  String? name;
  IconData? iconData;
  String? asset;

  CategoryProduct({this.name, this.iconData, this.asset});
}
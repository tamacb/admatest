import 'package:admatest/app/model/categories.dart';
import 'package:admatest/app/model/product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  //TODO: Implement HomeController

  final indexPage = 0.obs;

  final searchEditingController = TextEditingController();

  final categories = <CategoryProduct>[
    CategoryProduct(name: 'Art', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Design', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Coding', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Business', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Traveling', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Marketing', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'LifeStyle', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Art', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Design', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Coding', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Business', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Traveling', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'Marketing', iconData: Icons.image, asset: 'iconcat'),
    CategoryProduct(name: 'LifeStyle', iconData: Icons.image, asset: 'iconcat'),
  ].obs;

  final products = <Product>[
    Product(title: 'OTW Surabaya', isBestSeller: true, isFav: true, price: 180.0, rating: 4.9, tutor: 'Robert H'),
    Product(title: 'Gas tipis tipis', isBestSeller: false, isFav: false, price: 80.0, rating: 4.1, tutor: 'Dono H'),
    Product(title: 'Pecel Lele', isBestSeller: false, isFav: false, price: 980.0, rating: 4.3, tutor: 'Kasino H'),
    Product(title: 'Sari Laut', isBestSeller: true, isFav: false, price: 780.0, rating: 4.5, tutor: 'Indro H'),
    Product(title: 'OTW Surabaya', isBestSeller: true, isFav: true, price: 180.0, rating: 4.9, tutor: 'Robert H'),
    Product(title: 'Gas tipis tipis', isBestSeller: false, isFav: false, price: 80.0, rating: 4.1, tutor: 'Dono H'),
    Product(title: 'Pecel Lele', isBestSeller: false, isFav: false, price: 980.0, rating: 4.3, tutor: 'Kasino H'),
    Product(title: 'Sari Laut', isBestSeller: true, isFav: false, price: 780.0, rating: 4.5, tutor: 'Indro H'),
    Product(title: 'OTW Surabaya', isBestSeller: true, isFav: true, price: 180.0, rating: 4.9, tutor: 'Robert H'),
    Product(title: 'Gas tipis tipis', isBestSeller: false, isFav: false, price: 80.0, rating: 4.1, tutor: 'Dono H'),
    Product(title: 'Pecel Lele', isBestSeller: false, isFav: false, price: 980.0, rating: 4.3, tutor: 'Kasino H'),
    Product(title: 'Sari Laut', isBestSeller: true, isFav: false, price: 780.0, rating: 4.5, tutor: 'Indro H'),

  ].obs;

  final wishlist = <Product>[
    Product(title: 'OTW Surabaya', isBestSeller: true, isFav: true, price: 180.0, rating: 4.9, tutor: 'Robert H'),
    Product(title: 'Gas tipis tipis', isBestSeller: false, isFav: false, price: 80.0, rating: 4.1, tutor: 'Dono H'),
    Product(title: 'Pecel Lele', isBestSeller: false, isFav: false, price: 980.0, rating: 4.3, tutor: 'Kasino H'),
  ].obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}

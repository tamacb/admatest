import 'package:admatest/app/modules/home/controllers/home_controller.dart';
import 'package:admatest/app/widgets/badges/header_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfileView extends GetView<HomeController> {
  ProfileView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(25.0), color: Colors.white70),
              child: ListTile(
                trailing: const Icon(Icons.arrow_forward_ios),
                leading: Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(15.0), color: Colors.amberAccent),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Image.asset(
                      'assets/mbake.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                  ),
                ),
                title: Text('Your Name', style: GoogleFonts.dmSans(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),),
                subtitle: Text('YourName@mail.com', style: GoogleFonts.dmSans(
                  color: Colors.black,
                  fontSize: 13,
                  fontWeight: FontWeight.normal,
                ),),
              ),
            ),
          ),
          buildHeader(titleHeader: 'Account', left: 15.0),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(25.0), color: Colors.white70),
              child: Column(
                children: [
                  ListTile(
                    trailing: const Icon(Icons.arrow_forward_ios),
                    leading: Image.asset(
                      'assets/notifacc.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: const Text('Notifications'),
                  ),
                  ListTile(
                    trailing: const Icon(Icons.arrow_forward_ios),
                    leading: Image.asset(
                      'assets/security.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: const Text('Security'),
                  ),
                  ListTile(
                    trailing: const Icon(Icons.arrow_forward_ios),
                    leading: Image.asset(
                      'assets/emailpref.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: const Text('Email Preference'),
                  ),
                ],
              ),
            ),
          ),
          buildHeader(titleHeader: 'Course', left: 15.0),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(25.0), color: Colors.white70),
              child: Column(
                children: [
                  ListTile(
                    trailing: const Icon(Icons.arrow_forward_ios),
                    leading: Image.asset(
                      'assets/cert.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: const Text('Certificate'),
                  ),
                  ListTile(
                    trailing: const Icon(Icons.arrow_forward_ios),
                    leading: Image.asset(
                      'assets/pay.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: const Text('Payment'),
                  ),
                  ListTile(
                    trailing: const Icon(Icons.arrow_forward_ios),
                    leading: Image.asset(
                      'assets/histo.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: const Text('History'),
                  ),
                ],
              ),
            ),
          ),
          buildHeader(titleHeader: 'Support', left: 15.0),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(25.0), color: Colors.white70),
              child: Column(
                children: [
                  ListTile(
                    trailing: const Icon(Icons.arrow_forward_ios),
                    leading: Image.asset(
                      'assets/help.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: const Text('Help'),
                  ),
                  ListTile(
                    trailing: const Icon(Icons.arrow_forward_ios),
                    leading: Image.asset(
                      'assets/faq.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: const Text('FAQ'),
                  ),
                ],
              ),
            ),
          ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Align(
            alignment: Alignment.center,
            child: Text('Sign Out', style: GoogleFonts.dmSans(
              color: Colors.red,
              fontSize: 16,
              fontWeight: FontWeight.normal,
            ),),
          ),
        ),
      ],
    ),
        ));
  }
}

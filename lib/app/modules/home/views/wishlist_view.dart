import 'package:admatest/app/modules/home/controllers/home_controller.dart';
import 'package:admatest/app/widgets/product/item_product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';

class WishlistView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return AlignedGridView.count(
      crossAxisCount: 2,
      mainAxisSpacing: 4,
      crossAxisSpacing: 4,
      itemCount: controller.wishlist.length,
      itemBuilder: (context, index) {
        var item = controller.wishlist[index];
        return buildProductItem(
            isBestSeller: item.isBestSeller,
            isFav: item.isFav,
            price: item.price,
            rating: item.rating,
            title: item.title,
            tutor: item.tutor);
      },
    );
  }
}

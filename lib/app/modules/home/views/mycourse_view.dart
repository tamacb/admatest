import 'package:admatest/app/modules/home/controllers/home_controller.dart';
import 'package:admatest/app/modules/home/views/home_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

class MyCourseView extends GetView<HomeController> {
  @override
  final HomeController controller = Get.put(HomeController());

  MyCourseView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            buildTabCourse(),
            Expanded(
              child: TabBarView(children: [
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: controller.products.length,
                    itemBuilder: (context, index) => itemMyCourse(
                        title: controller.products[index].title,
                        tutor: controller.products[index].tutor)),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: controller.products.length,
                    itemBuilder: (context, index) => itemMyCourse(
                        title: controller.products[index].title,
                        tutor: controller.products[index].tutor)),
              ]),
            )
          ],
        ),
      ),
    );
  }

  Widget itemMyCourse({String? title, String? tutor}) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 8.0, right: 10.0),
      child: Card(
        shape: RoundedRectangleBorder(
          side: const BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Image.asset('assets/mbake.png'),
                    width: Get.width * 0.2,
                    height: Get.height * 0.1,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(15.0), color: Colors.amberAccent),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          '$title',
                          style: GoogleFonts.dmSans(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                          child: Row(
                            children: [
                              Image.asset(
                                'assets/people.png',
                                width: 15.0,
                                height: 15.0,
                              ),
                              Text(
                                '$tutor',
                                style: GoogleFonts.dmSans(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w100,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 50.0,
                          height: 20.0,
                          child: Center(
                            child: Text(
                              'label',
                              style: GoogleFonts.dmSans(
                                color: HexColor('#2F80ED'),
                                fontSize: 12,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ),
                          decoration:
                              BoxDecoration(color: HexColor('#DCF3F5'), borderRadius: BorderRadius.circular(50.0)),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Progress',
                            style: GoogleFonts.dmSans(
                              color: Colors.grey,
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            '20/29 lesson',
                            style: GoogleFonts.dmSans(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Due time',
                            style: GoogleFonts.dmSans(
                              color: Colors.grey,
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            'November 2, 2021',
                            style: GoogleFonts.dmSans(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Container(
                      height: 10.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Colors.grey[200],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Container(
                      height: 10.0,
                      width: 100.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: HexColor('#2F80ED'),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Padding buildTabCourse() {
    return Padding(
      padding: const EdgeInsets.only(left: 12.0, right: 12.0, bottom: 15.0, top: 15.0),
      child: Material(
        animationDuration: const Duration(seconds: 1),
        borderRadius: BorderRadius.circular(20.0),
        color: HexColor('#F4F4F4'),
        child: SizedBox(
          height: 50.0,
          child: TabBar(
            isScrollable: false,
            unselectedLabelStyle: GoogleFonts.dmSans(
              color: Colors.black,
              fontSize: 14,
              fontWeight: FontWeight.normal,
            ),
            indicatorColor: HexColor('#2F80ED'),
            tabs: const [
              Tab(
                text: 'Ongoing',
              ),
              Tab(
                text: 'Complete',
              ),
            ],
            padding: const EdgeInsets.all(5.0),
            unselectedLabelColor: Colors.black,
            labelColor: Colors.white,
            indicator: RectangularIndicator(
              color: HexColor('#2F80ED'),
              bottomLeftRadius: 15,
              bottomRightRadius: 15,
              topLeftRadius: 15,
              topRightRadius: 15,
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:admatest/app/common_utils.dart';
import 'package:admatest/app/modules/home/views/mycourse_view.dart';
import 'package:admatest/app/modules/home/views/profile_view.dart';
import 'package:admatest/app/modules/home/views/wishlist_view.dart';
import 'package:admatest/app/routes/app_pages.dart';
import 'package:admatest/app/widgets/badges/header_item.dart';
import 'package:admatest/app/widgets/input_field/inputTextFormFieldBase.dart';
import 'package:admatest/app/widgets/product/item_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Obx(() => Scaffold(
            appBar: renderBottomTabAppBar(context),
            body: renderBottomTabPage(context),
            bottomNavigationBar: BottomNavigationBar(
              showSelectedLabels: true,
              selectedLabelStyle: GoogleFonts.redHatDisplay(
                color: HexColor('#2F80ED'),
                fontSize: 14,
                fontWeight: FontWeight.normal,
              ),
              unselectedLabelStyle: GoogleFonts.redHatDisplay(
                color: HexColor('#CACACA'),
                fontSize: 14,
                fontWeight: FontWeight.normal,
              ),
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Image.asset(
                      'assets/${(controller.indexPage.value == 0) ? 'explorer' : 'explorerwh'}.png',
                      height: 24,
                      width: 24,
                    ),
                  ),
                  label: 'Explorer',
                ),
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Image.asset(
                      'assets/${(controller.indexPage.value == 1) ? 'playbtn' : 'mycourse'}.png',
                      height: 24,
                      width: 24,
                    ),
                  ),
                  label: 'My Course',
                ),
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Image.asset(
                      'assets/${(controller.indexPage.value == 2) ? 'heart' : 'wishlist'}.png',
                      height: 24,
                      width: 24,
                    ),
                  ),
                  label: 'Wishlist',
                ),
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Image.asset(
                      'assets/${(controller.indexPage.value == 3) ? 'accountac' : 'account'}.png',
                      height: 24,
                      width: 24,
                    ),
                  ),
                  label: 'Account',
                ),
              ],
              currentIndex: controller.indexPage.value,
              onTap: (index) {
                controller.indexPage.value = index;
                logger.i('index skrg ${controller.indexPage.value}');
              },
              selectedItemColor: HexColor('#2F80ED'),
              unselectedItemColor: HexColor('#CACACA'),
              showUnselectedLabels: true,
            ),
          )),
    );
  }

  SingleChildScrollView home(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          buildHeaderProduct(title: 'Popular Course'),
          Obx(
            () => SizedBox(
              height: Get.height * 0.38,
              child: ListView.builder(
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.COURSE_DETAIL);
                  },
                  child: buildProductItem(
                      title: controller.products[index].title,
                      isBestSeller: controller.products[index].isBestSeller,
                      tutor: controller.products[index].tutor,
                      isFav: controller.products[index].isFav,
                      price: controller.products[index].price,
                      rating: controller.products[index].rating),
                ),
                itemCount: controller.products.length,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
              ),
            ),
          ),
          buildHeaderProduct(title: 'Categories'),
          SizedBox(
            height: Get.height * 0.1,
            child: MasonryGridView.count(
              crossAxisCount: 2,
              mainAxisSpacing: 4,
              crossAxisSpacing: 4,
              scrollDirection: Axis.horizontal,
              itemCount: controller.categories.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Image.asset('assets/${controller.categories[index].asset}.png', width: 24.0,height: 24.0,),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        '${controller.categories[index].name}',
                        style: GoogleFonts.dmSans(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
          buildHeaderProduct(title: 'Your Topic', subHeader: true, titleSubHeader: 'Coding'),
          Obx(
            () => SizedBox(
              height: Get.height * 0.38,
              child: ListView.builder(
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.COURSE_DETAIL);
                  },
                  child: buildProductItem(
                      title: controller.products[index].title,
                      isBestSeller: controller.products[index].isBestSeller,
                      tutor: controller.products[index].tutor,
                      isFav: controller.products[index].isFav,
                      price: controller.products[index].price,
                      rating: controller.products[index].rating),
                ),
                itemCount: controller.products.length,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
              ),
            ),
          ),
          buildHeaderProduct(title: 'Your Topic', subHeader: true, titleSubHeader: 'Marketing'),
          Obx(
            () => SizedBox(
              height: Get.height * 0.38,
              child: ListView.builder(
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.COURSE_DETAIL);
                  },
                  child: buildProductItem(
                      title: controller.products[index].title,
                      isBestSeller: controller.products[index].isBestSeller,
                      tutor: controller.products[index].tutor,
                      isFav: controller.products[index].isFav,
                      price: controller.products[index].price,
                      rating: controller.products[index].rating),
                ),
                itemCount: controller.products.length,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
              ),
            ),
          ),
        ],
      ),
    );
  }

  PreferredSize appBarHome(BuildContext context) {
    return PreferredSize(
        preferredSize: Size.fromHeight(Get.height * 0.22),
        child: Container(
          child: Stack(
            children: [
              Positioned(
                  left: 25,
                  bottom: 40,
                  child: Image.asset(
                    'assets/lineappbar.png',
                    width: Get.width * 0.45,
                  )),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'hi, ADMA',
                                style: GoogleFonts.redHatDisplay(
                                  color: Colors.white,
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Image.asset(
                                  'assets/hi.png',
                                  height: 24.0,
                                  width: 24.0,
                                ),
                              )
                            ],
                          ),
                          Text(
                            'Let\'s start learning',
                            style: GoogleFonts.dmSans(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.asset(
                              'assets/cart.png',
                              height: 50.0,
                              width: 50.0,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              buildBottomSheetNotification();
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset(
                                'assets/notification.png',
                                height: 50.0,
                                width: 50.0,
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0, left: 15.0, top: 15.0),
                    child: InputTextFormFieldBase(
                      prefix: const Icon(Icons.search),
                      controller: controller,
                      hintText: 'Search for anything',
                      textEditingController: controller.searchEditingController,
                      obscureText: false,
                      passwordVisibility: false,
                    ),
                  )
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
              borderRadius:
                  const BorderRadius.only(bottomLeft: Radius.circular(20.0), bottomRight: Radius.circular(20.0)),
              color: HexColor('#2F80ED')),
        ));
  }

  PreferredSize appBarCourse(BuildContext context) {
    return PreferredSize(
        preferredSize: Size.fromHeight(Get.height * 0.22),
        child: Container(
          child: Stack(
            children: [
              Positioned(
                  left: 25,
                  bottom: 40,
                  child: Image.asset(
                    'assets/lineappbar.png',
                    width: Get.width * 0.45,
                  )),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          'My Course',
                          style: GoogleFonts.redHatDisplay(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const SizedBox()
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0, left: 15.0, top: 15.0),
                    child: InputTextFormFieldBase(
                      prefix: const Icon(Icons.search),
                      controller: controller,
                      hintText: 'Search for anything',
                      textEditingController: controller.searchEditingController,
                      obscureText: false,
                      passwordVisibility: false,
                    ),
                  )
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
              borderRadius:
                  const BorderRadius.only(bottomLeft: Radius.circular(20.0), bottomRight: Radius.circular(20.0)),
              color: HexColor('#2F80ED')),
        ));
  }

  PreferredSize appBarWishlist(BuildContext context) {
    return PreferredSize(
        preferredSize: Size.fromHeight(Get.height * 0.22),
        child: Container(
          child: Stack(
            children: [
              Positioned(
                  left: 25,
                  bottom: 40,
                  child: Image.asset(
                    'assets/lineappbar.png',
                    width: Get.width * 0.45,
                  )),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          'Wishlist',
                          style: GoogleFonts.redHatDisplay(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const SizedBox()
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0, left: 15.0, top: 15.0),
                    child: InputTextFormFieldBase(
                      prefix: const Icon(Icons.search),
                      controller: controller,
                      hintText: 'Search for anything',
                      textEditingController: controller.searchEditingController,
                      obscureText: false,
                      passwordVisibility: false,
                    ),
                  )
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
              borderRadius:
                  const BorderRadius.only(bottomLeft: Radius.circular(20.0), bottomRight: Radius.circular(20.0)),
              color: HexColor('#2F80ED')),
        ));
  }

  void buildBottomSheetNotification() {
    Get.bottomSheet(
        Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 30.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SizedBox(
                        width: 50.0,
                      ),
                      GestureDetector(
                        onTap: () {
                          Get.until((route) => Get.isBottomSheetOpen == false);
                        },
                        child: Container(
                          height: 50.0,
                          width: 50.0,
                          child: const Icon(Icons.close),
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
                      color: Colors.white),
                  width: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'Notification',
                              style: GoogleFonts.redHatDisplay(
                                color: Colors.black,
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Image.asset(
                                'assets/hi.png',
                                height: 24.0,
                                width: 24.0,
                              ),
                            )
                          ],
                        ),
                      ),
                      buildHeader(titleHeader: 'Today'),
                      itemCardNotification(
                          title: 'Your payment is success',
                          subtitle: 'Start your course now.',
                          iconAssets: 'checknotif'),
                      itemCardNotification(
                          title: 'Your payment is success', subtitle: 'Start your course now.', iconAssets: 'lonceng'),
                      buildHeader(titleHeader: 'Yesterday'),
                      itemCardNotification(
                          title: 'Your payment is failed', subtitle: 'apayaaa.', iconAssets: 'checknotif'),
                      itemCardNotification(
                          title: 'Your payment is success', subtitle: 'wkwkkwkwkwkwkkw.', iconAssets: 'lonceng'),
                      buildHeader(titleHeader: 'This Week'),
                      itemCardNotification(
                          title: 'Your payment is success',
                          subtitle: 'Start your course now.',
                          iconAssets: 'checknotif'),
                      itemCardNotification(
                          title: 'Your payment is success', subtitle: 'Start your course now.', iconAssets: 'lonceng'),
                      buildHeader(titleHeader: 'This Month'),
                      itemCardNotification(
                          title: 'Your payment is failed', subtitle: 'apayaaa.', iconAssets: 'checknotif'),
                      itemCardNotification(
                          title: 'Your payment is success', subtitle: 'wkwkkwkwkwkwkkw.', iconAssets: 'lonceng'),
                      buildHeader(titleHeader: 'This Year'),
                      itemCardNotification(
                          title: 'Your payment is failed', subtitle: 'apayaaa.', iconAssets: 'checknotif'),
                      itemCardNotification(
                          title: 'Your payment is success', subtitle: 'wkwkkwkwkwkwkkw.', iconAssets: 'lonceng'),
                    ],
                  ),
                )
              ],
            ),
          ),
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
              color: Colors.transparent),
        ),
        isScrollControlled: true,
        enableDrag: true);
  }

  Padding itemCardNotification({String? title, String? subtitle, String? iconAssets}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
          shape: RoundedRectangleBorder(
            side: const BorderSide(color: Colors.white70, width: 1),
            borderRadius: BorderRadius.circular(20),
          ),
          child: ListTile(
            leading: Image.asset(
              'assets/$iconAssets.png',
              width: 26,
              height: 24,
            ),
            title: Text(
              '$title',
              style: GoogleFonts.dmSans(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
            subtitle: Text(
              '$subtitle.',
              style: GoogleFonts.dmSans(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.normal,
              ),
            ),
            trailing: Icon(
              Icons.more_horiz,
              color: HexColor('#2F80ED'),
              size: 15,
            ),
          )),
    );
  }

  Row buildHeaderProduct({String? title, String? titleSubHeader, bool? subHeader = false}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                '$title',
                style: GoogleFonts.dmSans(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            if (subHeader!)
              Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(40.0), color: HexColor('#DCF3F5')),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Center(
                    child: Text(
                      '$titleSubHeader',
                      style: GoogleFonts.dmSans(
                        color: HexColor('#2F80ED'),
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'See all',
            style: GoogleFonts.dmSans(
              color: HexColor('#2F80ED'),
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ],
    );
  }

  Widget renderBottomTabPage(BuildContext context) {
    logger.wtf(controller.indexPage.value);
    var command = '${controller.indexPage.value}';
    switch (command) {
      case '0':
        return home(context);
      case '1':
        return MyCourseView();
      case '2':
        return WishlistView();
      case '3':
        return ProfileView();
      default:
        return const Text('0');
    }
  }

  PreferredSize renderBottomTabAppBar(BuildContext context) {
    logger.wtf(controller.indexPage.value);
    var command = '${controller.indexPage.value}';
    switch (command) {
      case '0':
        return appBarHome(context);
      case '1':
        return appBarCourse(context);
      case '2':
        return appBarWishlist(context);
      case '3':
        return appBarProfile();
      default:
        return PreferredSize(
            preferredSize: Size.fromHeight(Get.height * 0.22),
            child: Container(
              decoration: const BoxDecoration(
                  borderRadius:
                      BorderRadius.only(bottomLeft: Radius.circular(20.0), bottomRight: Radius.circular(20.0)),
                  color: Colors.blue),
            ));
    }
  }

  PreferredSize appBarProfile() {
    return PreferredSize(
          preferredSize: Size.fromHeight(Get.height * 0.10),
          child: AppBar(
            elevation: 0,
              backgroundColor: Colors.white,
              title: Padding(
            padding: const EdgeInsets.only(top: 20.0,left: 12.0),
            child: Text('Profile', style: GoogleFonts.redHatDisplay(
              color: Colors.black,
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),),
          )));
  }
}
import 'package:admatest/app/common_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

import '../controllers/course_detail_controller.dart';

class CourseDetailView extends GetView<CourseDetailController> {
  const CourseDetailView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(Get.height * 0.23),
            child: Container(
              child: Stack(
                children: [
                  Container(
                    height: Get.height * 0.4,
                    color: HexColor('#FFEA7D'),
                  ),
                  Image.asset(
                    'assets/lefttop.png',
                    width: 500,
                    height: 250.0,
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                    left: -20,
                    bottom: 10,
                    child: Image.asset(
                      'assets/dot.png',
                      width: 100,
                      height: 100.0,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    left: -20,
                    bottom: -120,
                    child: Transform(
                      child: Image.asset(
                        'assets/circlewhite.png',
                        width: 200,
                        height: 200.0,
                        fit: BoxFit.cover,
                      ),
                      alignment: FractionalOffset.center,
                      transform: Matrix4.identity()..rotateZ(5),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Get.back();
                          },
                          child: Image.asset(
                            'assets/backbtn.png',
                            width: 45,
                          ),
                        ),
                        Container(
                          height: 40.0,
                          width: 40.0,
                          child: Image.asset(
                            'assets/buy.png',
                            width: 10,
                            height: 10,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0), color: Colors.black.withOpacity(0.2)),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    bottom: 0,
                    child: Image.asset(
                      'assets/mbake.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Image.asset(
                      'assets/play.png',
                      width: 180.0,
                      height: 80.0,
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SizedBox(
                        height: 50,
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            height: 25.0,
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.only(topLeft: Radius.circular(35.0), topRight: Radius.circular(35.0))),
                          )),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  borderRadius:
                      const BorderRadius.only(bottomLeft: Radius.circular(20.0), bottomRight: Radius.circular(20.0)),
                  color: HexColor('#2F80ED')),
            )),
        body: Padding(
          padding: const EdgeInsets.only(left: 15.0, right: 15.0),
          child: Container(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Design Thinking Fundamental',
                      style: GoogleFonts.dmSans(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Flexible(
                          flex: 1,
                          child: Row(
                            children: [
                              Image.asset(
                                'assets/people.png',
                                height: 15,
                                width: 15,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  'Hello Academy',
                                  style: GoogleFonts.dmSans(
                                    color: HexColor('#2F80ED'),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w100,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: Row(
                            children: [
                              Image.asset(
                                'assets/rating.png',
                                height: 20.0,
                                width: 20.0,
                              ),
                              Text(
                                '4.8',
                                style: GoogleFonts.redHatDisplay(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: Text(
                      'Description this is a simple description that explain the description about the class or blabla bla and then blablabla of course.',
                      style: GoogleFonts.redHatDisplay(
                        color: Colors.grey,
                        fontSize: 14,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Students',
                                      style: GoogleFonts.redHatDisplay(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Text(
                                      '143.256',
                                      style: GoogleFonts.redHatDisplay(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Students',
                                      style: GoogleFonts.redHatDisplay(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Text(
                                      '143.256',
                                      style: GoogleFonts.redHatDisplay(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                      Padding(
                          padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Last update',
                                      style: GoogleFonts.redHatDisplay(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Text(
                                      'Feb 2, 2021',
                                      style: GoogleFonts.redHatDisplay(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Subtitle',
                                      style: GoogleFonts.redHatDisplay(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          'English and',
                                          style: GoogleFonts.redHatDisplay(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 5.0),
                                          child: Text(
                                            '5 more',
                                            style: GoogleFonts.redHatDisplay(
                                              color: Colors.blueAccent,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                  DefaultTabController(
                    length: 2,
                    initialIndex: 0,
                    child: Container(
                      color: Colors.transparent,
                      height: Get.height * 0.5,
                      child: Column(
                        children: [
                          buildTab(),
                          Expanded(
                            child: TabBarView(children: [
                              SingleChildScrollView(
                                child: Column(
                                  children: [
                                    itemCardNotification(title: '0', subtitle: '00.53 mins'),
                                    itemCardNotification(
                                        title: 'Design Thinking bukan Thingking', subtitle: '00.53 mins'),
                                    itemCardNotification(
                                        title: 'Design Thinking bukan Thingking', subtitle: '00.53 mins'),
                                  ],
                                ),
                              ),
                              SingleChildScrollView(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Align(alignment: Alignment.center, child: Text('Student Review', style: GoogleFonts.dmSans(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.normal,
                                    ),)),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          RatingBar.builder(
                                            itemSize: 20.0,
                                            initialRating: 3,
                                            minRating: 1,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                                            itemBuilder: (context, _) => const Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {
                                              logger.i(rating);
                                            },
                                          ),
                                          const Text('4.8 out 5'),
                                        ],
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                          elevation: 0,
                                          value: 0,
                                          icon: Image.asset(
                                            'assets/dropd.png',
                                            height: 15.0,
                                            width: 15.0,
                                          ),
                                          items: const <DropdownMenuItem<int>>[
                                            DropdownMenuItem(
                                              child: Text('sort by'),
                                              value: 0,
                                            ),
                                            DropdownMenuItem(
                                              child: Text('Relevan'),
                                              value: 1,
                                            ),
                                            DropdownMenuItem(
                                              child: Text('Oldest'),
                                              value: 2,
                                            ),
                                            DropdownMenuItem(
                                              child: Text('Newes'),
                                              value: 2,
                                            ),
                                          ],
                                          onChanged: (v) {},
                                        ),
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              width: 45.0,
                                              height: 45.0,
                                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(15.0), color: Colors.amber,),),
                                            const SizedBox(width: 5,),
                                            Column(
                                              children: [
                                                Text(
                                                  'Reviewer Username',
                                                  style: GoogleFonts.redHatDisplay(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w300,
                                                  ),
                                                ),
                                                Text(
                                                  '1 day ago',
                                                  style: GoogleFonts.redHatDisplay(
                                                    color: Colors.black,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w300,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Image.asset('assets/rating.png', height: 25.0,width: 25.0,),
                                            Text(
                                              '4.2',
                                              style: GoogleFonts.redHatDisplay(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w300,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        'Description this is a simple description that explain the description about the class or blabla bla.',
                                        style: GoogleFonts.redHatDisplay(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ]),
                          )
                        ],
                      ),
                    ),
                  ),
                  // Container(
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.spaceAround,
                  //     children: [
                  //       Expanded(
                  //         flex: 1,
                  //         child: Padding(
                  //           padding: const EdgeInsets.all(4.0),
                  //           child: GestureDetector(
                  //             onTap: () {
                  //               controller.indexOfTab.value = 0;
                  //             },
                  //             child: Container(
                  //               height: 45.0,
                  //               decoration: BoxDecoration(
                  //                   color: controller.indexOfTab.value == 0
                  //                       ? HexColor('#2F80ED')
                  //                       : HexColor('#F4F4F4'),
                  //                   borderRadius: BorderRadius.circular(15.0)),
                  //               child: Center(
                  //                   child: Text(
                  //                 'Curriculum',
                  //                 style: GoogleFonts.dmSans(
                  //                   color: Colors.black,
                  //                   fontSize: 16,
                  //                   fontWeight: FontWeight.w500,
                  //                 ),
                  //               )),
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //       Expanded(
                  //         flex: 1,
                  //         child: Padding(
                  //           padding: const EdgeInsets.all(4.0),
                  //           child: GestureDetector(
                  //             onTap: () {
                  //               controller.indexOfTab.value = 1;
                  //             },
                  //             child: Container(
                  //               height: 45.0,
                  //               decoration: BoxDecoration(
                  //                   color: controller.indexOfTab.value == 1
                  //                       ? HexColor('#2F80ED')
                  //                       : HexColor('#F4F4F4'),
                  //                   borderRadius: BorderRadius.circular(15.0)),
                  //               child: Center(
                  //                   child: Text(
                  //                 'Review',
                  //                 style: GoogleFonts.dmSans(
                  //                   color: Colors.black,
                  //                   fontSize: 16,
                  //                   fontWeight: FontWeight.w500,
                  //                 ),
                  //               )),
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  //   height: 45.0,
                  //   decoration:
                  //       BoxDecoration(color: HexColor('#F4F4F4'), borderRadius: BorderRadius.circular(15.0)),
                  // ),

                  // (controller.indexOfTab.value == 0)
                  //     ? Column(
                  //         children: [
                  //           itemCardNotification(title: '0', subtitle: '00.53 mins'),
                  //           itemCardNotification(title: 'Design Thinking bukan Thingking', subtitle: '00.53 mins'),
                  //           itemCardNotification(title: 'Design Thinking bukan Thingking', subtitle: '00.53 mins'),
                  //         ],
                  //       )
                  //     : Padding(
                  //         padding: const EdgeInsets.all(8.0),
                  //         child: Column(
                  //           mainAxisAlignment: MainAxisAlignment.center,
                  //           crossAxisAlignment: CrossAxisAlignment.center,
                  //           children: [
                  //             const Align(alignment: Alignment.center, child: Text('Student Review')),
                  //             Row(
                  //               mainAxisAlignment: MainAxisAlignment.center,
                  //               children: [
                  //                 RatingBar.builder(
                  //                   itemSize: 20.0,
                  //                   initialRating: 3,
                  //                   minRating: 1,
                  //                   direction: Axis.horizontal,
                  //                   allowHalfRating: true,
                  //                   itemCount: 5,
                  //                   itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                  //                   itemBuilder: (context, _) => const Icon(
                  //                     Icons.star,
                  //                     color: Colors.amber,
                  //                   ),
                  //                   onRatingUpdate: (rating) {
                  //                     logger.i(rating);
                  //                   },
                  //                 ),
                  //                 const Text('4.8 out 5'),
                  //               ],
                  //             )
                  //           ],
                  //         ),
                  //       ),
                ],
              ),
            ),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0), topRight: Radius.circular(30)),
                color: Colors.white),
          ),
        ),
        bottomNavigationBar: Material(
          elevation: 20,
          child: Container(
            height: 190.0,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Price',
                        style: GoogleFonts.dmSans(
                          color: Colors.grey,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        '\$ 150',
                        style: GoogleFonts.dmSans(
                          color: HexColor('#2F80ED'),
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                            onPressed: () {},
                            child: const Text('Enroll'),
                            style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                primary: HexColor('#2F80ED'),
                                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                                textStyle: GoogleFonts.dmSans(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: Text(
                              'Add To Cart',
                              style: GoogleFonts.dmSans(
                                color: HexColor('#2F80ED'),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                                side: BorderSide(color: HexColor('#2F80ED'), width: 1),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                primary: Colors.white,
                                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15)),
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          flex: 1,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: Text(
                              'Add To Wishlist',
                              style: GoogleFonts.dmSans(
                                color: HexColor('#2F80ED'),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                                side: BorderSide(color: HexColor('#2F80ED'), width: 1),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                primary: Colors.white,
                                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15)),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Padding buildTab() {
    return Padding(
      padding: const EdgeInsets.only(left: 12.0, right: 12.0, bottom: 15.0, top: 15.0),
      child: Material(
        animationDuration: const Duration(seconds: 1),
        borderRadius: BorderRadius.circular(20.0),
        color: HexColor('#F4F4F4'),
        child: SizedBox(
          height: 50.0,
          child: TabBar(
            isScrollable: false,
            unselectedLabelStyle: GoogleFonts.dmSans(
              color: Colors.black,
              fontSize: 14,
              fontWeight: FontWeight.normal,
            ),
            indicatorColor: HexColor('#2F80ED'),
            tabs: const [
              Tab(
                text: 'Curriculum',
              ),
              Tab(
                text: 'Review',
              ),
            ],
            padding: const EdgeInsets.all(5.0),
            unselectedLabelColor: Colors.black,
            labelColor: Colors.white,
            indicator: RectangularIndicator(
              color: HexColor('#2F80ED'),
              bottomLeftRadius: 20,
              bottomRightRadius: 20,
              topLeftRadius: 20,
              topRightRadius: 20,
            ),
          ),
        ),
      ),
    );
  }

  Padding itemCardNotification({String? title, String? subtitle, String? iconAssets}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
          shape: RoundedRectangleBorder(
            side: const BorderSide(color: Colors.white70, width: 1),
            borderRadius: BorderRadius.circular(20),
          ),
          child: ListTile(
            title: Text(
              '$title',
              style: GoogleFonts.dmSans(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
            subtitle: Text(
              '$subtitle.',
              style: GoogleFonts.dmSans(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.normal,
              ),
            ),
            trailing: Image.asset(
              'assets/playitem.png',
              width: 50.0,
              height: 50.0,
            ),
          )),
    );
  }

  Widget renderTabPage(BuildContext context) {
    logger.wtf(controller.indexOfTab.value);
    var command = '${controller.indexOfTab.value}';
    switch (command) {
      case '0':
        return const Text('0');
      case '1':
        return const Text('1');
      default:
        return Container();
    }
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CourseDetailController extends GetxController {
  //TODO: Implement CourseDetailController

  final count = 0.obs;
  final indexOfTab = 0.obs;
  late TabController _controller;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
